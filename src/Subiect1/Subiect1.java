package Subiect1;

public class Subiect1 {
    public static void main(String[] args) {
        L l = new L();
        K k = new K(l);
        I i = new I(123, k);
    }
}

interface Y {
    void f();
}

class I implements J, Y{
    private long t;
    private K k;

    public I(long t, K k) {
        this.t = t;
        this.k = k;
    }

    @Override
    public void f() {

    }

    @Override
    public void i(I i) {

    }
}

interface J {
    void i(I i);
}

class K {
    private M m = new M();
    private L l;

    public K(L l) {
        this.l = l;
    }
}

class M {
    public void meatB() {

    }
}

class L {
    public void metA() {

    }
}

class N {

}
