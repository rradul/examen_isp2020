package Subiect2;

import javax.swing.*;
import java.awt.*;

public class Subiect2 extends JFrame {
    private JTextArea jTextArea;
    private JTextArea jTextArea2;
    private String text;

    public Subiect2() throws HeadlessException {
        setSize(700, 700);
        JPanel panel = new JPanel();
        panel.setLayout(null);

        jTextArea = new JTextArea("AAAAA");
        this.text = jTextArea.getText();
        jTextArea.setBounds(20, 10, 100, 50);
        jTextArea.setEditable(false);
        jTextArea2 = new JTextArea("");
        jTextArea2.setBounds(20, 70, 100, 50);
        jTextArea2.setEditable(false);
        panel.add(jTextArea);
        panel.add(jTextArea2);

        JButton button = new JButton("Buton");
        button.setBounds(100, 100, 100, 50);
        panel.add(button);

        button.addActionListener(e -> Button());

        add(panel);
        setVisible(true);
    }

    private void Button() {
        jTextArea.setText("");
        jTextArea2.setText(text);
    }

    public static void main(String[] args) {
        Subiect2 s = new Subiect2();
    }
}
